  $(document).ready(function() {
    function setHeight() {
      windowHeight = $(window).innerHeight();
      $('.video-marque').css('min-height', windowHeight);
    };

    });

  
  
  $(window).scroll(function() {
  if ($(this).scrollTop() > 1){  
    $('header').addClass("sticky");
  }
  else {
    $('header').removeClass("sticky");
  }
});

$(document).ready(function(){
  $(".offcanvas__trigger--open").click(function(){
      $("body").addClass("disable_scroll");
  });
    $(".close_btn").click(function(){
        $("body").removeClass("disable_scroll");
    });
});

  $(document).ready(function() {
      $('#my-navigation').iptOffCanvas({
          baseClass: 'offcanvas',
          type: 'right' // top, right, bottom, left.
      });
  });
  $(document).ready(function(){
      $(".canvas-sidebar-open").click(function(){
          $(".search_canvas").addClass("offcanvas--right--active");
          $(".canvas-body").addClass("sidebar-overlay");
          $("#applyFilter").css("display","none");
      });
  });
  $(document).ready(function(){
      $(".close_sidebar").click(function(){
          $(".search_canvas").removeClass("offcanvas--right--active");
          $(".canvas-body").removeClass("sidebar-overlay");
          $("#applyFilter").css("display","inherit");
      });
  });
  $(document).ready(function(){
      $(".offcanvas__trigger--open").click(function(){
          $("#applyFilter").addClass("offcanvas--right--active");
      });
  });

  /* tickets user profile js here */
  $(document).ready(function(){

      $(".notify_others").click(function(){

          $(".notify_others_form").addClass("visible");

      });

  });


  jQuery(function($) {
      $('.report_height').responsiveEqualHeightGrid();
      $('.performance_height').responsiveEqualHeightGrid();

      var wow = new WOW(
          {
              boxClass:     'wow',      // default
              animateClass: 'animated', // default
              offset:       0,          // default
              mobile:       true,       // default
              live:         true        // default
          }
      )
      wow.init();
  });


  $(document).ready(function() {
      $('#my-navigation').iptOffCanvas({
          baseClass: 'offcanvas',
          type: 'right' // top, right, bottom, left.
      });
  });
  $(document).ready(function(){
      $(".canvas-sidebar-open").click(function(){
          $(".offcanvas").addClass("offcanvas--right--active");
          $(".canvas-body").addClass("sidebar-overlay");
      });
  });
  $(document).ready(function(){
      $(".close_sidebar").click(function(){
          $(".offcanvas").removeClass("offcanvas--right--active");
          $(".canvas-body").removeClass("sidebar-overlay");
      });
  });

  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
  $('#startDate').datepicker({
      uiLibrary: 'bootstrap4',
      iconsLibrary: 'fontawesome',
      minDate: today,
      maxDate: function () {
          return $('#endDate').val();
      }
  });
  $('#endDate').datepicker({
      uiLibrary: 'bootstrap4',
      iconsLibrary: 'fontawesome',
      minDate: function () {
          return $('#startDate').val();
      }
  });


  function createOptions(number) {
      var options = [], _options;

      for (var i = 0; i < number; i++) {
          var option = '<option value="' + i + '">Option ' + i + '</option>';
          options.push(option);
      }

      _options = options.join('');

      $('#number')[0].innerHTML = _options;
      $('#number-multiple')[0].innerHTML = _options;

      $('#number2')[0].innerHTML = _options;
      $('#number2-multiple')[0].innerHTML = _options;
  }
  var mySelect = $('#first-disabled2');
  createOptions(4000);
  $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
  });

  $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
  });

  $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
  });

  $(".search-btn").click(function () {

      $(".search-input-elm").animate({ width: 'toggle' });

  });


  $(document).ready(function() {
      $('#applyFilter').iptOffCanvas({
          baseClass: 'offcanvas',
          type: 'right' // top, right, bottom, left.
      });
  });

  /* js for form validation*/
  $(document).ready(function() {
      $('form[id="login_form"]').validate({
          rules: {
              firs_name: 'required',
              email: {
                  required: true,
                  email: true,
              },
              password: {
                  required: true,
                  minlength: 8,
              }
          },
          messages: {
              firs_name: 'This field is required',
              email: 'Enter a valid email',
              password: {
                  minlength: 'Password must be at least 8 characters long'
              }
          },
          submitHandler: function(form) {
              form.submit();
          }
      });
      $('form[id="signup_form"]').validate({
          rules: {
              full_name: 'required',
              email: {
                  required: true,
                  email: true,
              }
          },
          messages: {
              full_name: 'This field is required',
              email: 'Enter a valid email',
          },
          submitHandler: function(form) {
              form.submit();
          }
      });

  });
  /* // js for form validation*/





